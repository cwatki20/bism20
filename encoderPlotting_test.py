#encoderPlotting_test.py
#191202 code created to troupbleshoot plotting the trajectory computed by the encoders


# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2
import math
from numpy.linalg import inv
import serial
import matplotlib.pyplot as plt

#load input image
print("[INFO] Loading image")
frame = cv2.imread("191122_vertCheckerboard_test.png")
(h,w) = frame.shape[:2]

#Chessboard dimensions
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480

print("[INFO] Looking for chessboard corners")
ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)

#assign chessboard corners to image coordinates
if(ret == True):
    
    TL = corners[0]
    TR = corners[3]
    BL = corners[12]
    BR = corners[15]
    corners_img = np.array([TL, TR, BL, BR])

    #assign chessboard corners to world coordinates
    corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])

    #print("\nImage points\n")
    #print(corners_img)
    #print("\nWorld points\n")
    #print(corners_world)
    
    #Calculate Homography
    #H, status = cv2.findHomography(corners_world, corners_img)   CHANGE TO INVERT
    G, status = cv2.findHomography(corners_world, corners_img)
    #print(np.asarray(G))
    H = inv(G)

    #Print H matrix
    if H.any:
        H = np.asarray(H)
	G = np.asarray(G)
        print("[INFO] Chessboard corners found")
        #print(H)
        #print(G)

#Calculate distance from BR to TR of chessboard
"""
TR_H = np.append(TR, 1)
BR_H = np.append(BR, 1)
world_TR_H = np.array(np.matmul(H, TR_H))
world_BR_H = np.array(np.matmul(H, BR_H))
world_TR = np.array([world_TR_H[0]/world_TR_H[2], world_TR_H[1]/world_TR_H[2]])
world_BR = np.array([world_BR_H[0]/world_BR_H[2], world_BR_H[1]/world_BR_H[2]])
dist = cv2.norm(world_TR - world_BR) / scale
print(dist)
"""

#Display corners on image
lineColor = (255, 0, 0)
lineThickness = 2
TL_tup = tuple(TL[0])
TR_tup = tuple(TR[0])
BL_tup = tuple(BL[0])
BR_tup = tuple(BR[0])
frame = cv2.putText(frame, "TL", TL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "TR", TR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "BL", BL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "BR", BR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

 ################ Collision Prevention #################
# Read trajectory from Arduino via serial --- TODO: decouple this from person detection
# so that it still runs even if a person is not detected
    
v = float(1000) #Velocity in mm/s
rL = float(-10000) #Radii of curvature in mm
rR = float(-10747)

# Calculate if person's coordinates are within danger zone
collision = False
#wb = 152.4 #Wheelbase in mm
wb = 1235.0
#LHS = (x+wb)**2 + (y-(rR+rL)/2)**2
#RHS_inner = rR**2 + wb**2
#RHS_outer = rL**2 + wb**2
#if (LHS > RHS_inner and LHS < RHS_outer):
#	collision = True
#elif (rR == 0 and rL == 0):
#	if (x < wb/2 and x > -wb/2):
#		collision = True
#print("Collision imminent: ")
#print(collision)


################# Here i'm going to test transforming a point from worldreal to world #################
q_b = [2000*scale, 1000*scale]

# Find p_wb:
zeroX = w / 2
zeroY = h
zero = (zeroX, zeroY)
zeroH = np.array([zeroX, zeroY, 1])
world_zeroH = np.array(np.matmul(H, zeroH))
world_zero = np.array([world_zeroH[0]/world_zeroH[2]/scale, world_zeroH[1]/world_zeroH[2]/scale])
p_wb = world_zero

# Find R_wb:
world_x = ([1000*scale, 0])
world_xH = ([1000*scale, 0, 1])
img_xH = np.array(np.matmul(G, world_xH))
img_x = np.array([img_xH[0]/img_xH[2], img_xH[1]/img_xH[2]])
frame = cv2.line(frame, TL_tup, tuple(np.int_(img_x)), lineColor, lineThickness)

img_base_x = (zeroX, zeroY-100)
img_base_xH = ([zeroX, zeroY-100, 1])
frame = cv2.line(frame, zero, img_base_x, lineColor, lineThickness)
world_base_xH = np.array(np.matmul(H, img_base_xH))
world_base_x = np.array([world_base_xH[0]/world_base_xH[2]/scale, world_base_xH[1]/world_base_xH[2]/scale])
world_base_x_free = world_base_x - world_zero
theta = math.atan2(world_base_x_free[1], world_base_x_free[0]) - math.atan2(world_x[1], world_x[0])
#print("theta:")
#print(theta)
R_wb = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])
#print("p_wb:")
#print(p_wb)
#print("R_wb")
#print(R_wb)

# Compute g_bw from g_wb
p_bw = np.matmul(-R_wb.T, p_wb)
R_bw = R_wb.T

# Apply g_bw to q_b to get q_w
q_w = p_wb*scale + np.matmul(R_wb, q_b)

#print("q_w")
#print(q_w)

# Apply inverse homography to q_w display point
q_wH = ([q_w[0], q_w[1], 1])
world_qH = np.matmul(G, q_wH)
world_q = np.array([world_qH[0]/world_qH[2], world_qH[1]/world_qH[2]])
#print("world_q:")
#print(world_q)
frame = cv2.circle(frame, tuple(np.int_(world_q)), 2, lineColor, lineThickness)


######################## Plotting curves in image coordinate frame ########################
leftH = np.array([0, 0, 1])
rightH = np.array([640, 0, 1])
world_leftH = np.array(np.matmul(H, leftH))
world_rightH = np.array(np.matmul(H, rightH))
world_left = np.array([world_leftH[0]/world_leftH[2], world_leftH[1]/world_leftH[2]])
world_right = np.array([world_rightH[0]/world_rightH[2], world_rightH[1]/world_rightH[2]])
#base_left = p_bw*scale + np.matmul(R_bw, world_left)
#base_right = p_bw*scale + np.matmul(R_bw, world_right)

# Vector of points representing our curves in the base frame
spacing = 500
y_vec = np.linspace(-20000, 20000, spacing)
x_vec = np.sqrt(((rR+rL)/2)**2+wb**2-(y_vec+(rL+rR)/2)**2) - wb
vec = np.array([x_vec, y_vec])
#print(vec)


world_vec = []
for column in vec.T:
    #print(column)
    vec_iH = p_wb*scale + np.matmul(R_wb, column) #base to world
    vec_iH = ([vec_iH[0], vec_iH[1], 1])
    vec_iH = np.nan_to_num(np.array(np.matmul(G, vec_iH))) #world to image
    vec_i = np.int_(np.array([vec_iH[0]/vec_iH[2], vec_iH[1]/vec_iH[2]]))
    #world_vec = np.concatenate((world_vec, vec_i), axis=1)
    frame = cv2.circle(frame, tuple(vec_i), 2, lineColor, lineThickness)


# show the output frame
cv2.imshow("Frame", frame)
#cv2.imshow("Warped", frame_out)
cv2.waitKey(0)


# do a bit of cleanup
#cv2.destroyAllWindows()    
