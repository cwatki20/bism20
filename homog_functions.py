import numpy as np

def toHomogeneous(point):
    pointH = np.array([point[0], point[1], 1])
    return pointH

def fromHomogeneous(pointH):
    point = np.array([pointH[0]/pointH[2], pointH[1]/pointH[2]])
    return point

def applyHomography(point, H):
    pointH = toHomogeneous(point)
    transformed_pointH = np.nan_to_num(np.array(np.matmul(H, pointH)))
    transformed_point = fromHomogeneous(transformed_pointH)
    return transformed_point
