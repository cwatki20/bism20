# USAGE
# python chessboard_calibration.py

# Read from video stream until finding a chessboard,
# then write H matrix to file "homog.txt"

from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import imutils
import cv2
import argparse
import time
from numpy.linalg import inv

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", default="/home/nvidia/Documents/bism20/MobileNetSSD_deploy.prototxt",
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="/home/nvidia/Documents/bism20/MobileNetSSD_deploy.caffemodel",
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.05,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())


# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("[INFO] starting video stream...")
#vs = VideoStream("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)640, height=(int)480, format=(string)I420, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink").start()
#vs = VideoStream(usePiCamera=True).start()
vs = VideoStream('v4l2src device=/dev/video1 ! '
              'video/x-raw, width=(int)640, height=(int)480 ! '
               'videoconvert ! appsink').start()
time.sleep(2.0)
fps = FPS().start()

#Chessboard dimensions
#chess_w_mm = 125.0
#chess_h_mm = 175.0
#chess_w_mm = 291.2 #Larger chessboard
#chess_h_mm = 218.4
#sq_w = 36.4 #mm
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480


#Grab frames from the video stream until a chessboard is detected
#Chessboard detection temporarily commented out, homog is hardcoded
while True:
        #Read and display frame
        frame = vs.read()
        (h, w) = frame.shape[:2]
        #frameDisp = imutils.resize(frame, width = 400)
        #frame = imutils.resize(frame, width = 400)
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

        
        print("looking for chessboard corners")
        ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)

        
        if(ret == True):
                # Use the vertices in the chessboard to compute homography
                #corners_img = np.array(corners)
                #corners_world = []
                #for i in range(0,numCols):
                #        for j in range(0, numRows):
                #                corners_world.append([sq_w*scale*i,sq_w*scale*j])

                #corners_world = np.array(corners_world)

                TL = corners[0]
                BL = corners[3]
                TR = corners[12]
                BR = corners[15]
                corners_img = np.array([TL, TR, BL, BR])
                corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])
                
                # H is Homography that brings image to world, G is world to image
                G, status = cv2.findHomography(corners_world, corners_img)
                H = np.matrix(inv(G))

                if H.any:
                    print("Chessboad corners found")
                    print(np.asarray(H))
                    with open('homog.txt', 'wb') as f:
                        for line in H:
                            np.savetxt(f, line, fmt='%.8f')
                    break
