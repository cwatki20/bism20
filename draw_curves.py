import numpy as np
import math
import cv2

import matplotlib.pyplot as plt

from homog_functions import *

def plotCurves(frame, rR, rL, v, tw, wb, offset, t_stop, p_wb, R_wb, G, scale, x, y):

    
    offset = offset
    wb = wb    
    tw = tw
    rL = rL
    rR = rR
    
    """print("In draw_curves.py...")
    print("rL: ")
    print(rL)
    print("rR: ")
    print(rR)
    print("tw: ")
    print(tw)"""

    # If forklift is going in a straight line
    if rL == 0 and rR == 0:
        #Drawing line at end of danger zone
        x_lim = v*t_stop if abs(v) > 0 else 8000
        a = [x_lim, -tw/2]
        b = [x_lim, tw/2]
        
        a_world = p_wb*scale + np.matmul(R_wb, a)
        a_image = np.int_(applyHomography(a_world, G))
        
        b_world = p_wb*scale + np.matmul(R_wb, b)
        b_image = np.int_(applyHomography(b_world, G))
        
        frame = cv2.line(frame, tuple(a_image), tuple(b_image), (0,0,255), 2)
        
        spacing = 10
        x_vec = np.linspace(-40000, 40000, spacing)
        y_vecR = (tw/2)*np.ones(spacing)
        y_vecL = -y_vecR
        vecR = np.array([x_vec, y_vecR])
        vecL = np.array([x_vec, y_vecL])
        
        world_vecR = [0, 0]
        for column in vecR.T:
            world_vec_i = p_wb*scale + np.matmul(R_wb, column) #base to world
            world_vec_i = np.int_(applyHomography(world_vec_i, G))
            world_vecR = np.column_stack((world_vecR, world_vec_i))
        world_vecR = np.transpose(world_vecR)
        world_vecR = np.delete(world_vecR, (0), axis=0)
        frame = cv2.polylines(frame, [world_vecR], False, (0, 0 , 255), 2)
        
        world_vecL = [0, 0]
        for column in vecL.T:
            world_vec_i = p_wb*scale + np.matmul(R_wb, column) #base to world
            world_vec_i = np.int_(applyHomography(world_vec_i, G))
            world_vecL = np.column_stack((world_vecL, world_vec_i))
        world_vecL = np.transpose(world_vecL)
        world_vecL = np.delete(world_vecL, (0), axis=0)
        frame = cv2.polylines(frame, [world_vecL], False, (0, 0 , 255), 2)

    # If forklift is turning
    else:
        if rR+rL != 0: theta = 2*v*t_stop/(rR+rL)
        
        # Drawing line at end of danger zone
        if rR > rL:
            #Turning right
            a = [rL*np.sin(theta), rL*np.cos(theta)-(rR+rL)/2]
            b = [rR*np.sin(theta), rR*np.cos(theta)-(rR+rL)/2]
        #elif rL < rR:
        else:
            #Turning left
            a = [-rR*np.sin(math.pi-theta), -rR*np.cos(math.pi-theta)]
            b = [-rL*np.sin(math.pi-theta), -rL*np.cos(math.pi-theta)]
            
        a_world = p_wb*scale + np.matmul(R_wb, a)
        a_image = np.int_(applyHomography(a_world, G))
        
        b_world = p_wb*scale + np.matmul(R_wb, b)
        b_image = np.int_(applyHomography(b_world, G))
        
        
        frame = cv2.line(frame, tuple(a_image), tuple(b_image), (0,0,255), 2)
        
        
        spacing = 250
        y_vec = np.linspace(-20000, 20000, spacing)
        
        # Drawing both lines
        x_vecR = np.sqrt(rR**2+wb**2-(y_vec-(rR+rL)/2)**2) - wb - offset
        x_vecL = np.sqrt(rL**2+wb**2-(y_vec-(rR+rL)/2)**2) - wb - offset
        
        vecR = np.array([x_vecR, y_vec])
        vecL = np.array([x_vecL, y_vec])

        #figure = plt.figure()
        """plt.ion()
        plt.show()

        plt.axis([-20000, 20000, -20000, 20000])
        plt.plot(-y_vec, x_vecR, color='green')
        plt.plot(-y_vec, x_vecL, color='red')
        plt.plot(-y, x, 'o', color='black')
        plt.pause(1)"""
        
        
        world_vecR = [0, 0]
        for column in vecR.T:
            world_vec_i = p_wb*scale + np.matmul(R_wb, column) #base to world
            world_vec_i = np.int_(applyHomography(world_vec_i, G))
            world_vecR = np.column_stack((world_vecR, world_vec_i))
        world_vecR = np.transpose(world_vecR)
        world_vecR = np.delete(world_vecR, (0), axis=0)
        world_vecR = world_vecR[~np.all(world_vecR == 0, axis=1)]
        frame = cv2.polylines(frame, [world_vecR], False, (0, 255 , 0), 2)
        
        world_vecL = [0, 0]
        for column in vecL.T:
            world_vec_i = p_wb*scale + np.matmul(R_wb, column) #base to world
            world_vec_i = np.int_(applyHomography(world_vec_i, G))
            world_vecL = np.column_stack((world_vecL, world_vec_i))
        world_vecL = np.transpose(world_vecL)
        world_vecL = np.delete(world_vecL, (0), axis=0)
        world_vecL = world_vecL[~np.all(world_vecL == 0, axis=1)]
        frame = cv2.polylines(frame, [world_vecL], False, (0, 0 , 255), 2)

        
        
        ## Trouble shooting
        #i = np.int_((y + 20000)/250)
        #print ("input y")
        #print (y_vec[i])
        #print ("drawn x curve R:")
        #print (x_vecR[i])
        

    return frame
