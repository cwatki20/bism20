# USAGE
# python real_time_object_detection.py --prototxt MobileNetSSD_deploy.prototxt.txt --model MobileNetSSD_deploy.caffemodel

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2

from numpy.linalg import inv

#import GPIO
import RPi.GPIO as GPIO

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", default="MobileNetSSD_deploy.prototxt",
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="MobileNetSSD_deploy.caffemodel",
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.05,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("[INFO] starting video stream...")
#vs = VideoStream("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)640, height=(int)480, format=(string)I420, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink").start()
#vs = VideoStream(usePiCamera=True).start()
vs = VideoStream('v4l2src device=/dev/video1 ! '
              'video/x-raw, width=(int)640, height=(int)480 ! '
               'videoconvert ! appsink').start()
time.sleep(2.0)
fps = FPS().start()

#Chessboard dimensions
#chess_w_mm = 125.0
#chess_h_mm = 175.0
#chess_w_mm = 291.2 #Larger chessboard
#chess_h_mm = 218.4
#sq_w = 36.4 #mm
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480

# Pin definitions
#led_pin = 18 #board pin 12, BCM pin 18
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(led_pin, GPIO.OUT, initial=GPIO.LOW)

#Grab frames from the video stream until a chessboard is detected
while True:
        #Read and display frame
        frame = vs.read()
        #frameDisp = imutils.resize(frame, width = 400)
        #frame = imutils.resize(frame, width = 400)
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF
        
        print("looking for chessboard corners")
        ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)

        
        if(ret == True):
                # Use the vertices in the chessboard to compute homography
                #corners_img = np.array(corners)
                #corners_world = []
                #for i in range(0,numCols):
                #        for j in range(0, numRows):
                #                corners_world.append([sq_w*scale*i,sq_w*scale*j])

                #corners_world = np.array(corners_world)

                # ARE THESE CORRECT??
                TL = corners[0]
                BL = corners[3]
                TR = corners[12]
                BR = corners[15]
                corners_img = np.array([TL, TR, BL, BR])
                corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])
                
                #Homography that brings image to world
                G, status = cv2.findHomography(corners_world, corners_img)
                H = inv(G)

                #frame_out = cv2.warpPerspective(frame, H, cv2.size(1944, 2592))
                
                if H.any:
                        H = np.asarray(H)
                        print("Chessboard corners found")
                        print(H)
                        #print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in H]))
                        break
        
#GPIO.output(led_pin, GPIO.HIGH)
# loop over the frames from the video stream
while True:
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	frame = vs.read()
	#frame = imutils.resize(frame, width=400)

	# grab the frame dimensions and convert it to a blob
	(h, w) = frame.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
	#blob = cv2.dnn.blobFromImage(frame, 0.007843, (300, 300), 127.5)

	# pass the blob through the network and obtain the detections and
	# predictions
	net.setInput(blob)
	detections = net.forward()
        
	# loop over the detections
	for i in np.arange(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the prediction
		confidence = detections[0, 0, i, 2]

		# filter out weak detections by ensuring the `confidence` is
		# greater than the minimum confidence
		if confidence > args["confidence"]:
			# extract the index of the class label from the
			# `detections`, then compute the (x, y)-coordinates of
			# the bounding box for the object
			idx = int(detections[0, 0, i, 1])
			
                        #if the detection is a person, compute their distance from the camera
                        if CLASSES[idx] == "person":

                                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			        (startX, startY, endX, endY) = box.astype("int")
                                #Start is top left, end is bottom right
                                
			        # draw the prediction on the frame
			        label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
			        cv2.rectangle(frame, (startX, startY), (endX, endY),COLORS[idx], 2)
			        y = startY - 15 if startY - 15 > 15 else startY + 15
			        cv2.putText(frame, label, (startX, y),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                                
                                print("Person detected")

                                # first compute the position of the bottom of the box
                                #boxX = startX + (endX - startX) / 2
                                boxX = (startX + endX) / 2
                                boxY = endY
                                box = (boxX, boxY)
                                boxH = np.array([boxX, boxY, 1])
                                #boxH = cv2.convertPointsToHomogeneous(box)
                                #boxH = np.array([box, 1])
                                #print("Box X: {:.4f}".format(boxX))
                                #print("Box Y: {:.4f}".format(boxY))
                                
                                # position of bottom center of camera frame
                                zeroX = w / 2
                                zeroY = h
                                zero = (zeroX, zeroY)
                                zeroH = np.array([zeroX, zeroY, 1])
                                #zeroH = cv2.convertPointsToHomegeneous(zero)
                                #zeroH = np.array([zero, 1])

                                lineColor = (255, 0, 0)
                                lineThickness = 2
                                frame = cv2.line(frame, zero, box, lineColor, lineThickness)
                                
                                # compute distance from bottom center to box
                                #for j in box:
                                world_boxH = np.array(np.matmul(H, boxH))
                                world_zeroH = np.array(np.matmul(H, zeroH))
                                #world_box = cv2.convertPointsFromHomogeneous(world_boxH)
                                #world_zero = cv2.convertPointsFromHomogeneous(world_zeroH)
                                #print(world_boxH)
                                #print(world_zeroH)

                                #print(world_boxH.shape)
                                world_box = np.array([world_boxH[0]/world_boxH[2], world_boxH[1]/world_boxH[2]])
                                world_zero = np.array([world_zeroH[0]/world_zeroH[2], world_zeroH[1]/world_zeroH[2]])
                                
                                #Draw on start and end points
                                #start = (startX, startY)
                                #end = (endX, endY)
                                #frame = cv2.putText(frame, "start", start, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
                                #frame = cv2.putText(frame, "end", end, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

                                # Switched from displaying distances to displaying x,y coordinates
                                #dist = cv2.norm(world_box, world_zero) / scale
                                #print(dist)
                                #frame = cv2.putText(frame, str(dist), box, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
                                
                                x = (world_zero[0] - world_box[0]) / scale
                                y = (world_zero[1] - world_box[1]) / scale
                                pos = np.array([x,y])
                                print(pos)
                                posStr = "({0:f}. {1:f})".format(x,y)
                                frame = cv2.putText(frame, posStr, box, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

                        

	# show the output frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

	# update the FPS counter
	fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
