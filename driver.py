import RPi.GPIO as GPIO
import time
import os
import subprocess

# Pin Definitions
arduino_input_pin = 16 # Pin 15 on J21 corresponds to pin 22 on RPi pinout
#tx2_status_pin = 23 # Pin 16 on J21 corresponds to pin 23 on RPi pinout
#Pin setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(arduino_input_pin, GPIO.IN)

current_time = 0

while True:
    arduino_input = GPIO.input(arduino_input_pin)
    
    if(arduino_input == GPIO.HIGH):
        print("Reading high from duin")
        execfile('/home/nvidia/Documents/bism20/collision_prevention.py')
        current_time = 0
    else:
        print("Reading low from duin")
        current_time += 0.2
        
    if(current_time > 10):
        os.system('./put_to_linode')
        #edge case: FL turns back on while putting to Linode
        arduino_input2 = GPIO.input(arduino_input_pin)
        if(arduino_input2 == GPIO.LOW):
            print("Shutting down")
            os.system('systemctl poweroff')
        
    time.sleep(.2)

if __name__ == '__main__':
    main()
