# Charlie Watkins 191115
# Script which computes homography using chessboard, runs person detection, and
# computes their distance in a single image

#test_img: distance ~= 65cm

# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input image")
ap.add_argument("-p", "--prototxt", default="MobileNetSSD_deploy.prototxt",
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="MobileNetSSD_deploy.caffemodel",
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# Load input image and construct input blob
print("[INFO] Loading image")
frame = cv2.imread(args["image"])
(h,w) = frame.shape[:2]


#Chessboard dimensions
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480


print("looking for chessboard corners")
ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)


if(ret == True):
    # Use the vertices in the chessboard to compute homography
    #corners_img = np.array(corners)
    #corners_world = []
    #for i in range(0,numCols):
    #        for j in range(0, numRows):
    #                corners_world.append([sq_w*scale*i,sq_w*scale*j])
    
    #corners_world = np.array(corners_world)
    
    # ARE THESE CORRECT??
    TL = corners[0]
    TR = corners[3]
    BL = corners[12]
    BR = corners[15]
    corners_img = np.array([TL, TR, BL, BR])
    corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])
    
    #Homography that brings image to world
    H, status = cv2.findHomography(corners_world, corners_img)

    frame_out = cv2.warpPerspective(frame, H, (frame.shape[1], frame.shape[0]))
    
    if H.any:
        H = np.asarray(H)
        print("Chessboard corners found")
        print(H)
        #print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in H]))


blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
print("[INFO] computing object detections...")
net.setInput(blob)
detections = net.forward()
    
# loop over the detections
for i in np.arange(0, detections.shape[2]):
    # extract the confidence (i.e., probability) associated with
    # the prediction
    confidence = detections[0, 0, i, 2]
    
    # filter out weak detections by ensuring the `confidence` is
    # greater than the minimum confidence
    if confidence > args["confidence"]:
	# extract the index of the class label from the
	# `detections`, then compute the (x, y)-coordinates of
	# the bounding box for the object
	idx = int(detections[0, 0, i, 1])
	
        #if the detection is a person, compute their distance from the camera
        if CLASSES[idx] == "person":
            
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
	    (startX, startY, endX, endY) = box.astype("int")
            #Start is top left, end is bottom right
            
	    # draw the prediction on the frame
	    label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
	    cv2.rectangle(frame, (startX, startY), (endX, endY),COLORS[idx], 2)
	    y = startY - 15 if startY - 15 > 15 else startY + 15
	    cv2.putText(frame, label, (startX, y),
			cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
            
            print("Person detected")
            
            # first compute the position of the bottom of the box
            #boxX = startX + (endX - startX) / 2
            boxX = (startX + endX) / 2
            boxY = endY
            box = (boxX, boxY)
            boxH = np.array([boxX, boxY, 1])
            #boxH = cv2.convertPointsToHomogeneous(box)
            #boxH = np.array([box, 1])
            #print("Box X: {:.4f}".format(boxX))
            #print("Box Y: {:.4f}".format(boxY))
            
            # position of bottom center of camera frame
            zeroX = w / 2
            zeroY = h
            zero = (zeroX, zeroY)
            zeroH = np.array([zeroX, zeroY, 1])
            #zeroH = cv2.convertPointsToHomegeneous(zero)
            #zeroH = np.array([zero, 1])
            
            lineColor = (255, 0, 0)
            lineThickness = 2
            frame = cv2.line(frame, zero, box, lineColor, lineThickness)
            
            # compute distance from bottom center to box
            #for j in box:
            world_boxH = np.array(np.dot(H, boxH))
            world_zeroH = np.array(np.dot(H, zeroH))
            #world_box = cv2.convertPointsFromHomogeneous(world_boxH)
            #world_zero = cv2.convertPointsFromHomogeneous(world_zeroH)
            #print(world_boxH)
            #print(world_zeroH)
            
            #print(world_boxH.shape)
            world_boxH.resize(2,)
            world_zeroH.resize(2,)
            #print(world_boxH.shape)
            
            #print(world_boxH)
            #print(world_zeroH)
            
            
            #Draw on start and end points
            #start = (startX, startY)
            #end = (endX, endY)
            #frame = cv2.putText(frame, "start", start, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
            #frame = cv2.putText(frame, "end", end, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
            
            # Switched from displaying distances to displaying x,y coordinates
            #dist = cv2.norm(world_boxH, world_zeroH) / scale
            #dist = cv2.norm(world_boxH, world_zeroH)
            #print(dist)
            #frame = cv2.putText(frame, str(dist), box, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

            #Calculate distance from BR to TR of chessboard
            TR_H = np.append(TR, 1)
            BR_H = np.append(BR, 1)
            world_TR_H = np.array(np.dot(H, TR_H))
            world_BR_H = np.array(np.dot(H, BR_H))
            #dist = cv2.norm(world_TR_H, world_BR_H)
            print(world_TR_H)
            print(world_BR_H)
            #print(dist)

            #Display corners on image
            TL_tup = tuple(TL[0])
            TR_tup = tuple(TR[0])
            BL_tup = tuple(BL[0])
            BR_tup = tuple(BR[0])
            frame = cv2.putText(frame, "TL", TL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
            frame = cv2.putText(frame, "TR", TR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
            frame = cv2.putText(frame, "BL", BL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
            frame = cv2.putText(frame, "BR", BR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
          
            
            
            
            x = (world_boxH[0] - world_zeroH[0])
            y = (world_boxH[1] - world_zeroH[1])
            pos = np.array([x,y])
            print(pos)
            posStr = "({0:f}, {1:f})".format(x,y)
            frame = cv2.putText(frame, posStr, box, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

                        
	# show the output frame
	cv2.imshow("Frame", frame)
        #cv2.imshow("Warped", frame_out)
	cv2.waitKey(0)


# do a bit of cleanup
#cv2.destroyAllWindows()
