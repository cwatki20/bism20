#simple_homography.py
#Collin Meissner on 191119
#This is the simplist homography code without any person detection running

#import necesary packages
import numpy as np
import argparse
import imutils
import cv2

from numpy.linalg import inv

## construct the argumetn parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input image")
args = vars(ap.parse_args())

#load input image
print("[INFO] Loading image")
frame = cv2.imread(args["image"])
(h,w) = frame.shape[:2]

#Chessboard dimensions
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480

print("[INFO] Looking for chessboard corners")
ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)

#assign chessboard corners to image coordinates
if(ret == True):
    
    TL = corners[0]
    TR = corners[3]
    BL = corners[12]
    BR = corners[15]
    corners_img = np.array([TL, TR, BL, BR])

    #assign chessboard corners to world coordinates
    corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])

    print("\nImage points\n")
    print(corners_img)
    print("\nWorld points\n")
    print(corners_world)
    
    #Calculate Homography
    #H, status = cv2.findHomography(corners_world, corners_img)   CHANGE TO INVERT
    G, status = cv2.findHomography(corners_world, corners_img)
    print(np.asarray(G))
    H = inv(G)

    #Print H matrix
    if H.any:
        H = np.asarray(H)
        print("[INFO] Chessboard corners found")
        print(H)

#Calculate distance from BR to TR of chessboard
TR_H = np.append(TR, 1)
BR_H = np.append(BR, 1)
print(TR_H)
print(BR_H)
world_TR_H = np.array(np.matmul(H, TR_H))
world_BR_H = np.array(np.matmul(H, BR_H))
print(world_TR_H)
print(world_BR_H)
world_TR = np.array([world_TR_H[0]/world_TR_H[2], world_TR_H[1]/world_TR_H[2]])
world_BR = np.array([world_BR_H[0]/world_BR_H[2], world_BR_H[1]/world_BR_H[2]])
dist = cv2.norm(world_TR - world_BR) / scale
print(world_TR)
print(world_BR)
print(dist)

#Display corners on image
lineColor = (255, 0, 0)
lineThickness = 2
TL_tup = tuple(TL[0])
TR_tup = tuple(TR[0])
BL_tup = tuple(BL[0])
BR_tup = tuple(BR[0])
frame = cv2.putText(frame, "TL", TL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "TR", TR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "BL", BL_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)
frame = cv2.putText(frame, "BR", BR_tup, cv2.FONT_HERSHEY_PLAIN, 1, lineColor, lineThickness)

# show the output frame
cv2.imshow("Frame", frame)
#cv2.imshow("Warped", frame_out)
cv2.waitKey(0)


# do a bit of cleanup
#cv2.destroyAllWindows()    
