# USAGE
# Encoder plotting test with live video feed

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2
from numpy.linalg import inv
import serial
import math

#import GPIO
import RPi.GPIO as GPIO

ser = serial.Serial('/dev/ttyACM0', 9600, timeout=2)

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", default="MobileNetSSD_deploy.prototxt",
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="MobileNetSSD_deploy.caffemodel",
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.05,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())


# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("[INFO] starting video stream...")
#vs = VideoStream("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)640, height=(int)480, format=(string)I420, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink").start()
#vs = VideoStream(usePiCamera=True).start()
vs = VideoStream('v4l2src device=/dev/video1 ! '
              'video/x-raw, width=(int)640, height=(int)480 ! '
               'videoconvert ! appsink').start()
time.sleep(2.0)
fps = FPS().start()

#Chessboard dimensions
#chess_w_mm = 125.0
#chess_h_mm = 175.0
#chess_w_mm = 291.2 #Larger chessboard
#chess_h_mm = 218.4
#sq_w = 36.4 #mm
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480

# Pin definitions
#led_pin = 18 #board pin 12, BCM pin 18
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(led_pin, GPIO.OUT, initial=GPIO.LOW)
#fourcc = cv2.VideoWriter_fourcc(*'mpeg')
fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
out = cv2.VideoWriter('outpy5.avi', fourcc, 20, (640,480),True)

#Grab frames from the video stream until a chessboard is detected
while True:
        #Read and display frame
        frame = vs.read()
        (h, w) = frame.shape[:2]
        #frameDisp = imutils.resize(frame, width = 400)
        #frame = imutils.resize(frame, width = 400)
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF
        
        print("looking for chessboard corners")
        ret, corners = cv2.findChessboardCorners(frame, (numCols,numRows), None)

        
        if(ret == True):
                # Use the vertices in the chessboard to compute homography
                #corners_img = np.array(corners)
                #corners_world = []
                #for i in range(0,numCols):
                #        for j in range(0, numRows):
                #                corners_world.append([sq_w*scale*i,sq_w*scale*j])

                #corners_world = np.array(corners_world)

                # ARE THESE CORRECT??
                TL = corners[0]
                BL = corners[3]
                TR = corners[12]
                BR = corners[15]
                corners_img = np.array([TL, TR, BL, BR])
                corners_world = np.array([[0.0,0.0], [chess_w_mm*scale, 0.0], [0.0,chess_h_mm*scale], [chess_w_mm*scale,chess_h_mm*scale]])
                
                # H is Homography that brings image to world
                G, status = cv2.findHomography(corners_world, corners_img)
                H = inv(G)

                #frame_out = cv2.warpPerspective(frame, H, cv2.size(1944, 2592))
                
                if H.any:
                        H = np.asarray(H)
                        G = np.asarray(G)
                        print("Chessboard corners found")
                        #print(H)
                        #print(G)
                        break

lineColor = (255, 0, 0)
lineThickness = 2
##################################### Finding transformation from world to base frame ###################################
# Find p_wb:
zeroX = w / 2
zeroY = h
zero = (zeroX, zeroY)
zeroH = np.array([zeroX, zeroY, 1])
world_zeroH = np.array(np.matmul(H, zeroH))
world_zero = np.array([world_zeroH[0]/world_zeroH[2]/scale, world_zeroH[1]/world_zeroH[2]/scale])
p_wb = world_zero

# Find R_wb:
world_x = ([1000*scale, 0])
world_xH = ([1000*scale, 0, 1])
img_xH = np.array(np.matmul(G, world_xH))
img_x = np.array([img_xH[0]/img_xH[2], img_xH[1]/img_xH[2]])
frame = cv2.line(frame, tuple(TL[0]), tuple(np.int_(img_x)), lineColor, lineThickness)

img_base_x = (zeroX, zeroY-100)
img_base_xH = ([zeroX, zeroY-100, 1])
frame = cv2.line(frame, zero, img_base_x, lineColor, lineThickness)
world_base_xH = np.array(np.matmul(H, img_base_xH))
world_base_x = np.array([world_base_xH[0]/world_base_xH[2]/scale, world_base_xH[1]/world_base_xH[2]/scale])
world_base_x_free = world_base_x - world_zero
theta = math.atan2(world_base_x_free[1], world_base_x_free[0]) - math.atan2(world_x[1], world_x[0])
#print("theta:")
#print(theta)
R_wb = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])
#print("p_wb:")
#print(p_wb)
#print("R_wb")
#print(R_wb)

# Compute g_bw from g_wb (inverse)
p_bw = np.matmul(-R_wb.T, p_wb)
R_bw = R_wb.T



#GPIO.output(led_pin, GPIO.HIGH)
# loop over the frames from the video stream
while True:
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	frame = vs.read()
	#frame = imutils.resize(frame, width=400)

	# grab the frame dimensions and convert it to a blob
	(h, w) = frame.shape[:2]
        ################ Collision Prevention #################
        # Read trajectory from Arduino via serial --- TODO: decouple this from person detection
        # so that it still runs even if a person is not detected
        ser.flushInput()

        while True:
                line = ser.readline()
                words = line.split()
                if (len(words) == 6) and (words[0] == "v:"):
                        break
                print("Bad input")
        print(line)
        frame = cv2.putText(frame, line, (0,15), cv2.FONT_HERSHEY_PLAIN, 1 ,lineColor, lineThickness)
        v = float(words[1]) #Velocity in mm/s
        rL = float(words[3]) #Radii of curvature in mm
        rR = float(words[5])

        wb = 1325.0 #wheel base in mm
        tw = 1500.0*scale #track width in mm
        
        ####################### CURVE PLOTTING #######################
        # Plotting curves in image coordinate frame
        rL = rL*scale;
        rR = rR*scale;

        t_stop = 10.0

        if rL == 0 and rR == 0:

                #Drawing line at end of danger zone
                x_lim = v*t_stop if abs(v) > 0 else 8000
                a = [x_lim, -tw/2]
                b = [x_lim, tw/2]
                a_world = p_wb*scale + np.matmul(R_wb, a)
                a_worldH = ([a_world[0], a_world[1], 1])
                a_imageH = np.nan_to_num(np.array(np.matmul(G, a_worldH)))
                a_image = np.int_(np.array([a_imageH[0]/a_imageH[2], a_imageH[1]/a_imageH[2]]))

                b_world = p_wb*scale + np.matmul(R_wb, b)
                b_worldH = ([b_world[0], b_world[1], 1])
                b_imageH = np.nan_to_num(np.array(np.matmul(G, b_worldH)))
                b_image = np.int_(np.array([b_imageH[0]/b_imageH[2], b_imageH[1]/b_imageH[2]]))

                frame = cv2.line(frame, tuple(a_image), tuple(b_image), (0,0,255), 2)
                
                
                spacing = 10
                x_vec = np.linspace(-40000, 40000, spacing)
                y_vecR = (tw/2)*np.ones(spacing)
                y_vecL = -y_vecR
                vecR = np.array([x_vec, y_vecR])
                vecL = np.array([x_vec, y_vecL])
                
                world_vecR = [0, 0]
                for column in vecR.T:
                        vec_iH = p_wb*scale + np.matmul(R_wb, column) #base to world
                        vec_iH = ([vec_iH[0], vec_iH[1], 1])
                        vec_iH = np.nan_to_num(np.array(np.matmul(G, vec_iH))) #world to image
                        vec_i = np.int_(np.array([vec_iH[0]/vec_iH[2], vec_iH[1]/vec_iH[2]]))
                        world_vecR = np.column_stack((world_vecR, vec_i))
                world_vecR = np.transpose(world_vecR)
                world_vecR = np.delete(world_vecR, (0), axis=0)
                frame = cv2.polylines(frame, [world_vecR], False, (0, 0 , 255), 2)

                world_vecL = [0, 0]
                for column in vecL.T:
                        vec_iH = p_wb*scale + np.matmul(R_wb, column) #base to world
                        vec_iH = ([vec_iH[0], vec_iH[1], 1])
                        vec_iH = np.nan_to_num(np.array(np.matmul(G, vec_iH))) #world to image
                        vec_i = np.int_(np.array([vec_iH[0]/vec_iH[2], vec_iH[1]/vec_iH[2]]))
                        world_vecL = np.column_stack((world_vecL, vec_i))
                world_vecL = np.transpose(world_vecL)
                world_vecL = np.delete(world_vecL, (0), axis=0)
                frame = cv2.polylines(frame, [world_vecL], False, (0, 0 , 255), 2)
        else:
                if rR+rL != 0:
                        theta = 2*v*t_stop/(rR+rL)
                
                # Drawing line at end of danger zone
                if rR > rL:
                        #Turning right
                        a = [rL*np.sin(theta), rL*np.cos(theta)-(rR+rL)/2]
                        b = [rR*np.sin(theta), rR*np.cos(theta)-(rR+rL)/2]
                elif rL < rR:
                        #Turning left
                        a = [-rR*np.sin(math.pi-theta), -rR*np.cos(math.pi-theta)]
                        b = [-rL*np.sin(math.pi-theta), -rL*np.cos(math.pi-theta)]

                a_world = p_wb*scale + np.matmul(R_wb, a)
                a_worldH = ([a_world[0], a_world[1], 1])
                a_imageH = np.nan_to_num(np.array(np.matmul(G, a_worldH)))
                a_image = np.int_(np.array([a_imageH[0]/a_imageH[2], a_imageH[1]/a_imageH[2]]))

                b_world = p_wb*scale + np.matmul(R_wb, b)
                b_worldH = ([b_world[0], b_world[1], 1])
                b_imageH = np.nan_to_num(np.array(np.matmul(G, b_worldH)))
                b_image = np.int_(np.array([b_imageH[0]/b_imageH[2], b_imageH[1]/b_imageH[2]]))

                frame = cv2.line(frame, tuple(a_image), tuple(b_image), (0,0,255), 2)
                
                
                spacing = 250
                y_vec = np.linspace(-20000, 20000, spacing)
                offset = 1730.0
                #offset = 0
                
                # Drawing both lines
                x_vecR = np.sqrt(rR**2+wb**2-(y_vec+(rR+rL)/2)**2) - wb - offset
                x_vecL = np.sqrt(rL**2+wb**2-(y_vec+(rR+rL)/2)**2) - wb - offset
                
                vecR = np.array([x_vecR, y_vec])
                vecL = np.array([x_vecL, y_vec])
                
                world_vecR = [0, 0]
                for column in vecR.T:
                        vec_iH = p_wb*scale + np.matmul(R_wb, column) #base to world
                        vec_iH = ([vec_iH[0], vec_iH[1], 1])
                        vec_iH = np.nan_to_num(np.array(np.matmul(G, vec_iH))) #world to image
                        vec_i = np.int_(np.array([vec_iH[0]/vec_iH[2], vec_iH[1]/vec_iH[2]]))
                        world_vecR = np.column_stack((world_vecR, vec_i))
                world_vecR = np.transpose(world_vecR)
                world_vecR = np.delete(world_vecR, (0), axis=0)
                world_vecR = world_vecR[~np.all(world_vecR == 0, axis=1)]
                frame = cv2.polylines(frame, [world_vecR], False, (0, 0 , 255), 2)

                world_vecL = [0, 0]
                for column in vecL.T:
                        vec_iH = p_wb*scale + np.matmul(R_wb, column) #base to world
                        vec_iH = ([vec_iH[0], vec_iH[1], 1])
                        vec_iH = np.nan_to_num(np.array(np.matmul(G, vec_iH))) #world to image
                        vec_i = np.int_(np.array([vec_iH[0]/vec_iH[2], vec_iH[1]/vec_iH[2]]))
                        world_vecL = np.column_stack((world_vecL, vec_i))
                world_vecL = np.transpose(world_vecL)
                world_vecL = np.delete(world_vecL, (0), axis=0)
                world_vecL = world_vecL[~np.all(world_vecL == 0, axis=1)]
                #print(world_vecL)
                frame = cv2.polylines(frame, [world_vecL], False, (0, 0 , 255), 2)

        
	#Save the output frame
	#output = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
	out.write(frame)

	# show the output frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

	# update the FPS counter
	fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
