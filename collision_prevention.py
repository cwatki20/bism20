
# USAGE
# python collision_prevention.py

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2
from numpy.linalg import inv
import serial
import math
import threading
import sys
import os

from sympy import symbols
from sympy.plotting import plot as symplot

from playsound import playsound

# name csv file
timestr = time.strftime("%Y%m%d-%H%M%S")
#timestr = "Log%s.txt"%(timestr)
csvname = "/media/nvidia/SD17/Log%s.txt"%(timestr)
it = 1
#Begin thread
#def background():
#	while True:
#	    if raw_input() == 'event':
#		f = open(csvname,"a+")
#		eventTime = time.strftime("%H%M%S")
#		f.write("event occured at %s,\n" % (eventTime))
#		f.close()
#	    else:
#		print 'input did not equal "event"'

# threading1 runs regardless of user input
#threading1 = threading.Thread(target=background)
#threading1.daemon = True
#threading1.start()



#importing our functions
from homog_functions import *
from draw_curves import *


# DONT FORGET TO UNCOMMENT WHEN USING ARDUINO INPUTS
#ser = serial.Serial('/dev/ttyACM0', 9600, timeout=2)

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", default="/home/nvidia/Documents/bism20/MobileNetSSD_deploy.prototxt",
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="/home/nvidia/Documents/bism20/MobileNetSSD_deploy.caffemodel",
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.05,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())


# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
#print("[INFO] starting video stream...")
#vs = VideoStream("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)640, height=(int)480, format=(string)I420, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink").start()
#vs = VideoStream(usePiCamera=True).start()

# To check for video devices, type "ls -ltr /dev/video*" in terminal
print("[INFO] Initializing video stream...")
vs = VideoStream('v4l2src device=/dev/video0 ! '
              'video/x-raw, width=(int)640, height=(int)480 ! '
               'videoconvert ! appsink').start()

print("Done initializing video stream")
time.sleep(2.0)
fps = FPS().start()

#Chessboard dimensions
#chess_w_mm = 125.0
#chess_h_mm = 175.0
#chess_w_mm = 291.2 #Larger chessboard
#chess_h_mm = 218.4
#sq_w = 36.4 #mm
scale = 10
numCols = 4
numRows = 4
chess_w_mm = 480
chess_h_mm = 480

#Writing output video to file
#make string of current date/time for file name
#timestr = time.strftime("%Y%m%d-%H%M%S")
videoname = "/media/nvidia/SD17/%s.avi"%(timestr)
#videoname = "%s.avi"%(timestr)
print(videoname)
fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
out = cv2.VideoWriter(videoname, fourcc, 2, (640,480),True)


#Homography for camera mounted above tx2 in team room
#H = [[3.00939676, 87.7696912, -29149.6126], [73.2802662, 6.19292955, -23611.8018], [.00160203422, .00418265293, -.0819414340]]
#G = inv(H)

# Now reading Homography matrix from file, as obtained from calibration scrip
with open('/home/nvidia/Documents/bism20/homog.txt', 'r') as f:
        H = [[float(num) for num in line.strip().split(' ')] for line in f]
print(H)
H = np.asarray(H)
G = inv(H)


frame = vs.read()
(h, w) = frame.shape[:2]

font = cv2.FONT_HERSHEY_PLAIN
lineColor = (255, 0, 0)
lineThickness = 2

###################### Finding transformation from world to base frame ########################
# Find p_wb:
zero = (w/2, h)
world_zero = applyHomography(zero, H) / scale
p_wb = world_zero

# Find R_wb:
world_x = ([1000*scale, 0])
img_x = applyHomography(world_x, G)
img_base_x = (zero[0], zero[1]-100)
world_base_x = applyHomography(img_base_x, H)/scale
world_base_x_free = world_base_x - world_zero
theta = math.atan2(world_base_x_free[1], world_base_x_free[0]) - math.atan2(world_x[1], world_x[0])
R_wb = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])

# Compute g_bw from g_wb (inverse)
p_bw = np.matmul(-R_wb.T, p_wb)
R_bw = R_wb.T

aa = 1



############################# GPIO Stuff ###########################
#import GPIO
import RPi.GPIO as GPIO
# Pin definitions
arduino_input_pin = 16 #board pin 12, BCM pin 18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(arduino_input_pin, GPIO.IN)


### Play audio to indicate that person detection is running ###
playsound('/home/nvidia/Documents/bism20/beep-3.wav')
playsound('/home/nvidia/Documents/bism20/beep-3.wav')
playsound('/home/nvidia/Documents/bism20/beep-3.wav')



########################### Loop over the frames from the video stream ############################
while True:

        # If arduino input is low, break out of while loop and end program
        arduino_input = GPIO.input(arduino_input_pin)
        print("Arduino input: ")
        print(arduino_input)
        if (arduino_input == GPIO.LOW):
                print("breaking out of loop")
                break
        
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	frame = vs.read()
	#frame = imutils.resize(frame, width=400)

	# grab the frame dimensions and convert it to a blob
	(h, w) = frame.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
	#blob = cv2.dnn.blobFromImage(frame, 0.007843, (300, 300), 127.5)

	# pass the blob through the network and obtain the detections and
	# predictions
	net.setInput(blob)
	detections = net.forward()
        
        #default values for logging to CSV
        person = False
        v = np.nan
        collision = False
        
	# loop over the detections
	for i in np.arange(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the prediction
		confidence = detections[0, 0, i, 2]

		# filter out weak detections by ensuring the `confidence` is
		# greater than the minimum confidence
		if confidence > args["confidence"]:
			# extract the index of the class label from the
			# `detections`, then compute the (x, y)-coordinates of
			# the bounding box for the object
			idx = int(detections[0, 0, i, 1])
			
                        #if the detection is a person, compute their distance from the camera
                        if CLASSES[idx] == "person":

                                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			        (startX, startY, endX, endY) = box.astype("int")
                                #Start is top left, end is bottom right
                                
			        # draw the prediction on the frame
			        label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
			        cv2.rectangle(frame, (startX, startY), (endX, endY),COLORS[idx], 2)
			        y = startY - 15 if startY - 15 > 15 else startY + 15
			        cv2.putText(frame, label, (startX, y),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                                
                                print("Person detected")
                                #indicate there is a person detected
                                person = True
                                
                                # first compute the position of the bottom of the box
                                box = ((startX+endX)/2, endY)
                                # position of bottom center of camera frame (zero) computed above
                                
                                frame = cv2.line(frame, zero, box, lineColor, lineThickness)
                                
                                world_box = applyHomography(box, H)
                                world_zero = applyHomography(zero, H)
                                
                                # x = (world_zero[0] - world_box[0]) / scale
                                # y = (world_zero[1] - world_box[1]) / scale
                                # I think this is wrong...going to try to do it with homog. trans.

                                pos = p_bw + np.matmul(R_bw, world_box/scale)
                                x = pos[0]
                                y = pos[1]
                                
                                #pos = np.array([x,y])
                                posStr = "({0:f}. {1:f})".format(x,y)
                                frame = cv2.putText(frame, posStr, box, font, 1, lineColor, lineThickness)


                                ################ Collision Prevention #################
                                # Read trajectory from Arduino via serial
                                #TODO: decouple this from person detection
                                #so that it still runs even if a person is not detected
                          #      ser.flushInput()
                          #      while True:
                          #              line = ser.readline()
                          #              words = line.split()
                          #              if (len(words) == 6) and (words[0] == "v:"):
                          #                      break
                          #              print("Bad input")
                          #      print(line)
                          #      frame = cv2.putText(frame, line, (0,15), font, 1 ,lineColor, lineThickness)
                          #      v = float(words[1]) #Velocity in mm/s
                          #      rL = float(words[3])*scale #Radii of curvature in mm
                          #      rR = float(words[5])*scale

                                # Calculate if person's coordinates are within danger zone
                                collision = False
                                #wb = 152.4 #Wheelbase in mm
                                #wb = 1235.0
                                wb = 1325.0*scale
                                tw = 820*scale
                                t_stop = 10.0
                                offset = 600*scale

                                #Fake radii and velocity for testing
                                #Left Turn
                                #rR = (20000*scale+tw/2)
                                #rL = (20000*scale-tw/2)
                                #Right Turn
                                rR = -(20000*scale-tw/2)
                                rL = -(20000*scale+tw/2)
                                #rR = 0
                                #rL = 0
                                v = 1000

                                x = x*scale
                                y = y*scale
                                
                                """print("In collision_prevention.py...")
                                print("rL: ")
                                print(rL)
                                print("rR: ")
                                print(rR)
                                print("tw: ")
                                print(tw)"""
                                print "Position: ({0:f}, {1:f})".format(x,y)
                                
                                
                                if(rR == 0 and rL == 0):
                                        print("going straight")
                                        if(y < tw/2 and y > -tw/2):
                                                x_lim = v*t_stop if abs(v) > 0 else 8000
                                                if(x < x_lim/scale):
                                                        collision = True
                                else:
                                        print("turning")
                                        
                                        if rR+rL != 0: theta = 2*v*t_stop/(rR+rL)
                                        
                                        
                                        if (abs(rR) > abs(rL)): #left turn        
                                                print("Left turn")

                                                # NOTE THAT Y NEEDS TO BE NEGATIVE FOR SOME REASON
                                                x_curveR = (np.sqrt(rR**2+wb**2-(y-(rR+rL)/2)**2)-wb-offset)
                                                x_curveL = (np.sqrt(rL**2+wb**2-(y-(rR+rL)/2)**2)-wb-offset)
                                                
                                                a = [rL*np.sin(theta), rL*np.cos(theta)-(rR+rL)/2]
                                                b = [rR*np.sin(theta), rR*np.cos(theta)-(rR+rL)/2]
                                                line_eq = (a[0]-b[0])/(a[1]-b[1])*(y-a[1])+a[0]
                                                 
                                                if(x < x_curveR and (np.isnan(x_curveL) or x > x_curveL)):
                                                        if(x < line_eq):
                                                                collision = True
                                                                
                                        elif (abs(rR) < abs(rL)): #right turn
                                                print("Right turn")

                                                x_curveR = np.sqrt(rR**2+wb**2-(y-(rR+rL)/2)**2)-wb-offset
                                                x_curveL = np.sqrt(rL**2+wb**2-(y-(rR+rL)/2)**2)-wb-offset
                                                
                                                #a = [-rR*np.sin(math.pi-theta), -rR*np.cos(math.pi-theta)]
                                                #b = [-rL*np.sin(math.pi-theta), -rL*np.cos(math.pi-theta)]
                                                a = [rL*np.sin(theta), rL*np.cos(theta)-(rR+rL)/2]
                                                b = [rR*np.sin(theta), rR*np.cos(theta)-(rR+rL)/2]
                                                line_eq = (a[0]-b[0])/(a[1]-b[1])*(y-a[1])+a[0]

                                                if(x < x_curveL and (x > x_curveR or np.isnan(x_curveR))):
                                                        if(x < line_eq):
                                                                collision = True
                                                
                                        
                                print("Collision imminent: ")
                                print(collision)


                                #Displaying some points for debugging
                                p1 = np.array([0, 0])
                                p2 = np.array([20000, 0])
                                p1_world = p_wb*scale + np.matmul(R_wb, p1)
                                p1_img = np.int_(applyHomography(p1_world, G))
                                p2_world = p_wb*scale + np.matmul(R_wb, p2)
                                p2_img = np.int_(applyHomography(p2_world, G))
				p1_img = tuple(p1_img)
				p2_img = tuple(p2_img)
				#print(p1_img)

                                frame = cv2.circle(frame, p1_img, 5, (0, 0, 255))
                                frame = cv2.circle(frame, p2_img, 5, (0, 0, 255))
                                
                                # Play audio if in collision
                                if collision:
                                        #print("playing sound")
                                        playsound('/home/nvidia/Documents/bism20/beep-02.wav')
                                
                                ####################### CURVE PLOTTING #######################
                                # Plotting curves in image coordinate frame - see file draw_curves.py

                                #Plotting stuff in base coordinates
                                

                                #frame = plotCurves(frame, rR*scale, rL*scale, v, tw*scale, wb*scale, offset*scale, t_stop, p_wb, R_wb, G, scale, x, y)
                                
                                frame = plotCurves(frame, rR, rL, v, tw, wb, offset, t_stop, p_wb, R_wb, G, scale, x, y)
                                
                                                
	#Save the output frame
	out.write(frame)


        
        #log time to csv
        f = open(csvname, "a+")
        eventTime = time.strftime("%H%M%S")
        f.write("%s,%s,%f,%s,%s,\n" % (it,eventTime,v,person,collision))
        it = it+1
        f.close()
        
	# show the output frame
	#cv2.imshow("Frame", frame)
	#key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	#if key == ord("q"):
	#	break

	# update the FPS counter
	fps.update()


# stop the timer and display FPS information
fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
f.close()
cv2.destroyAllWindows()
vs.stop()
vs.stream.release()
