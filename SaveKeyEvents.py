# SaveKeyEvents.py
# Saves video 15 seconds before and after button press

# import the necessary packages
import KeyClipWriter
from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2

button == "pressed"
output = "videooutput.avi"
picamera = -1
fps = 20
codec = "MJPG"
bufferSize = 32

vs = VideoStream('v4l2src device=/dev/video1 ! '
              'video/x-raw, width=(int)640, height=(int)480 ! '
               'videoconvert ! appsink').start()
time.sleep(2.0)
fps = FPS().start()

while True:
	frame = vs.read()
	frame = imutils.resize(frame, width=600)
	updateConsecFrames = True

	
	if button == "pressed"
		if not kcw.recording:
			timestamp = datetime.datetime.now()
			P = "{}/{}.avi".format(output)
				timestamp.strftime("%Y%m%d-%H%M%S"))
			kcw.start(p, cv2.VideoWriter_fourcc(codec), fps)

	if updateConsecFrames:
		consecFrames += 1

	kcw.update(frame)

	if kcw.recording and consecFrames == args["buffer_size"]:
		kcw.finish()
	# show the frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break
# if we are in the middle of recording a clip, wrap it up
if kcw.recording:
	kcw.finish()
# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()




